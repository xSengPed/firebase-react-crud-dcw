import firebase from "firebase/app";
import "firebase/firestore";

const config = {
  apiKey: "AIzaSyBtG6I1_p9gF5P_uSqKJ-Gn7L16WwG3j2Y",
  authDomain: "client-react-crud-training.firebaseapp.com",
  databaseURL: "https://client-react-crud-training.firebaseio.com",
  projectId: "client-react-crud-training",
  storageBucket: "client-react-crud-training.appspot.com",
  messagingSenderId: "772562469703",
  appId: "1:772562469703:web:c0c4f3609381735ff44262",
  measurementId: "G-YEJ7CS8P18",
};

export default config;
