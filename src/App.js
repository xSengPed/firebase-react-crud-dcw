import React, { useState, useEffect } from "react";
// import logo from './logo.svg';
import { firestore } from "./index";
import "./App.css";

function App() {
  const [tasks, setTasks] = useState([
    {
      id: 1,
      name: "Do Homework",
    },
    {
      id: 2,
      name: "Write Go",
    },
  ]);
  const [name, setName] = useState("");

  const retriveData = () => {
    firestore.collection("tasks").onSnapshot((snapshot) => {
      console.log(snapshot.docs);
      let myTask = snapshot.docs.map((d) => {
        const { id, name } = d.data();

        console.log(id, name);
        return { id, name }; // retunr {id:id , name : name}
      });
      setTasks(myTask);
    });
  };
  useEffect(() => {
    retriveData();
  }, []);
  const addTask = () => {
    let id = (tasks.length === 0)? 1 : tasks[tasks.length - 1].id + 1;
    firestore.collection("tasks").doc(id+'').set({id,name});
  };

  const deleteTask = (id) => { // delete
    firestore.collection("tasks").doc(id+'').delete()
  }
  const editTask = (id) => {
    firestore.collection("tasks").doc(id+'').set({id,name})
  }
  const renderTask = () => {
    if (tasks && tasks.length) {
      return tasks.map((task, index) => {
        return (
          <li key={index}>
            {task.id} : {task.name}
            <button onClick={()=>deleteTask(task.id)}>delete</button>
            <button onClick={()=>editTask(task.id)}>edit</button>
          </li>
        );
      });
    } else {
      return(
        <div>No Tasks</div>
      )
    }

    
  };
  return (
    <div>
      <h1>Todo - List</h1>
      <input
        type="text"
        name="name"
        onChange={(e) => {
          setName(e.target.value);
        }}
      />
      <button onClick={addTask}>Add</button>
      <ul>{renderTask()}</ul>
    </div>
  );
}

export default App;
